# Projet final: Des enfants de 7 ans dans un musée de design.

## Introduction.

Pour ce projet, nous travaillons avec le musée de design de Bruxelles, the ADAM museum.
L'objectif de ce projet est de mettre en place une interface qui rendrait le musée accessible et compréhensible pour des enfants de 7 ans.

## Première idée.

Ma première intention a été de faire comprendre les différentes méthodes de fabrication des objets.
On peut en répertorier 5 différentes.

- **injection**

Le moulage par injection consiste à faire chauffer le plastique, jusqu'à ce qu'il fonde, pour ensuite l'injecter dans un moule. On laisse ensuite la matière refroidir et durcir avant de décoffrer.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/Injection_molding_diagram.svg.png)

Différentd objets fabriqués selon cette méthode:

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/objets%20injection.png)

- **extrusion**

Pour cette méthode, on injecte le plastique fondu dans le moule où une vis sans fin va venir pousser la matière vers la tête d'extrusion où le plastique va sortir et refroidir ou fur et à mesure qu'il s'extrude.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/extrusion.jpg)

Différents objets fabriqués selon cette méthode:

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/objets%20extrusion.png)

- **soufflage**

Lors d'un soufflage, on place le plastique fondu à l'entrée du moule creux et on vient injecter de l'air comme pour un ballon dans le but que le plastique épouse les parois intérieures du moule avant de refroidir.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/soufflage.jpg)

Différents objets fabriqués selon cette méthode:

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/objets%20soufflage.png)

- **rotomoulage**

Dans ce cas-ci, le plastique fondu est placé dans un moule que l'on va mettre en rotation afin que le plastique se place de manière homogène sur les parois intérieures.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/rotomoulage.png)

Différents objets fabriqués selon cette méthode:

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/objets%20roto.png)

- **thermoformage**

Cette méthode est la seule ou le plastique n'est pas fondu mais juste chauffé afin qu'il soit maléable. Une fois la plastique chauffé, on injecte de l'air pour faire gonfler le plastique et on place le moule en dessous. Une fois le moule placé, on retire l'air et le plastique va venir s'adapter à la forme du moule.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/Thermoformage.jpg)

Différents objets fabriqués selon cette méthode:

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/objets%20thermo.png)

Pour plus d'informations sur ces différentes méthodes, je vous conseille la vidéo de ["c'est pas sorcier"](https://www.youtube.com/watch?v=irFEnEZhlNM&t=2s) sur le thème du plastique que je trouve très complète et intéressante.

## Premiers tests.

Pour ce premier test, j'ai décidé de faire comprendre le principe d'extrusion au travers de la fabrication de la Dondolo.

J'ai donc commencé par modélisé une boîte creuse où l'on retrouve, creusé sur une des faces, le profil de la Dondolo. L'objectif était de placer une pâte dans cette boîte pour ensuite l'extruder par la fente en applicant une pression avec un deuxième élément dont les dimensions correspondent au vide de la première boîte.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/vert%20ouvert%20plus%20pate.jpg)

Malheureusement pour ce premier test les deux parties du moule ne s'emboitaient pas bien car je n'avais pas prévu une assez grande marge d'erreur.
J'ai donc du poncé pour rattraper cette erreur que j'ai corrigé sur le modèle en réduisant la pièce mâle de 1mm de chaque coté afin que tout s'emboite parfaitement.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/poncage.png)

Avant de commencer mes premiers tests, j'ai du recouvrir le moule de farine car le plastique est légèrement rugueu et la pâte à sucre avec laquelle j'ai décidé de travailler est légèrement collante.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/rugueu%20donc%20farine.png)

Je pouvais donc enfin débuter mes premiers tests. Mais j'ai très vite rencontré deux autres problèmes.

D'une part, la pâte avec laquelle j'ai décidé de travailler reste trop dur et pas assez maléable pour s'extruder correctement. J'ai donc essayé avec des serre-joints, mais même cela ne me permettait pas d'appliquer une assez grande force.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/pate%20dur.png)

D'une autre part, la paroi où se trouve la fente s'est avérée trop fine se qui rendait la zone dans la boucle de la fente hyper fragile. Comme on peut voir sur les photos, cette zone se déforme à la moindre pression que l'on applique. J'ai donc réimprimé cette pièce en triplant l'épaisseur de cette paroi mais elle se déforme encore légèrement et ne résiste toujours pas suffisament à la pression.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/fragile.png)

## Deuxième phase.

Après ces premiers tests et une première rencontre avec les enfants au musée, nous avons agrandi les groupes des différents projets. Pour ma part, ![Amina](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/amina.belguendouz/-/tree/main/docs) et ![Mailys](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/mailys.lechtchev/-/tree/main/docs) ont rejoind mon groupe.

Nous avons après une réflexion de groupe décidé de nous occuper de 3 des méthodes de fabricationexplicitées plus haut car nous n'avaons pas reussi à trouver pour les autres méthodes un moyen simple de les mettre en place afin qu'elles soient compréhensibles pour les enfants. Les 3 méthodes sur lesquelles nous avons donc décidé de nous concentrer sont l'injection, l'extrusion et le rotomoulage.

### L'injection.

Pour nos premiers tests de moule miniature à injection, nous avons décidé d'utiliser un pisto-colle avec lequel on est venu injecter la colle dans un moule en forme d'étoile où le système d'encoches se limitait à 4 points à la jonction entre les deux parties du moule. Ces 4 points n'étaient pas du tout solide et on cédés avant même le premier essaie. La forme d'étoile nous a aussi semblée trop compliquée à réaliser pour l'instant nous avons donc décidé de revenir à une forme plus simple.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/injection%20etoile.png)

Nous avons dés lors créé notre deuxième prototype, pour lequel nous avons amélioré le système d'accroche et nous lui avons choisi comme forme à mouler, un rond beaucoup plus simple à réaliser.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/injection%20rond.png)

Pour la suite de cette méthode, nous avons décidé que nous allions essayer d'utiliser le même matériau pour les 3 méthodes. Nous avons donc commencer à tester nos moules à injection avec de la cire.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/injection%20cire%20test1.png)

### L'extrusion.

Pour cette méthode, nous avons démarré des premiers tests que j'avais réalisé et que l'on retrouve ci-dessus.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/fragile.png)

Le gros problème que nous avons rencontré ici était la fragilité de la partie centrale de la paroie ou l'extrusion a lieu. Pour résoudre cela nous avons tenté plusieurs choses. D'abord, nous avons changer la forme pour qu'elle soit beaucoup plus simple, nous somme donc passé du profil de la chaise Dondolo à un rectangle ou un triangle.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/extrusion%20care.png)

Nous avons également tenté de changer la matérialité de la paroie où a lieu l'extrusion en changeant le design du moule imprimé en 3D afin de pouvoir y glisser un plaquette en plexi épais ou encore en bois.

Une autre modification que nous avons apporté a été de rallonger les moules tout en réduisant au maximum la surface pleine de la face d'extrusion, dans le but d'optimiser l'efficacité de nos moules.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/extrusion%20plaquette.png)

Nous avons également, avec comme objectif de lier toutes les méthodes, décidé de tenter l'extrusion avec la cire que nous avons donc du chauffer mais pas trop peu en tentant d'obtenir une pâte malaxable mais le résultat n'était concluant que trop peu longtemps et ne pemettait donc pas de réaliser l'extrusion.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/video-malaxage_6B2enn0Z.mp4)

L'extrusion éxtrusion étant trop compliquée avec de la cire, nous somme revenu a l'utilisation de la pâte fimo pour une plus grande facilité de manipulation.

Après plusieurs test de moule, nous avons remarqué que la pression nécessaire à l'extrusion pouvait par moments ètre relativement élevée, ce qui rend l'objet inutilisable pour un enfant de 7 ans. Nous avons donc mis en place un système de levier pour permettre aux enfants de décupler leur force et utiliser nos moules avec plus d'aisance.

### Le rotomoulage.

Pour cette méthode, nous avons pris comme référence, (video rotomoulage).

Nous avons donc décidé de recréer cette machine pour faire comprendre aux enfants le principe du rotomoulage qui nécessite une rotation à 360°.
Pour notre premier test, nous avons utiliser du bois contreplaqué que nous avons trouvé sous forme de chute dans l'atelier.
Pour le moule au centre, nous avons décidé de représenter une pokeball de Pokemon qui nous semblait être à la fois une forme simple à réaliser et aussi un objet qui permet de capter l'attention des enfants. Nous avons d'abbord tenté de travailler avec du plâtre mais les résultats n'étaient pas du tout concluants.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/roto%20machine%201.png)

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/pokeball%20platre.png)

Le type de bois utilisé pour ce premier test n'était pas le bon car certain vissage ou forage abîmaient ou faisaient craqueler le bois. Nous avons donc décidé de changer de type de bois et de nous diriger vers du mdf qui nous permettait une meilleure manipulation pour la suite de nos tests. Nous avons également lors de cette mise à jour du prototype, pris la décision d'arrondir les angles pour des questions de sécurité avec les enfants.
L'autre grande modification que nous avons réalisé à cette étape a été de changer de matériau pour se diriger vers de la cire. Cette fois-ci les résultats sont beaucoup plus concluants.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/roto%20machine%202.png)

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/moule%20pokeball.png)

Après que les enfants de Decroly soient venus aux ateliers pour tester nos objets, nous avons constaté que l'activation du méchanisme posait problème pour les enfants car cela leur demandait d'être fort délicat ce qui n'était le cas de tous. Pour résoudre cela, nous avons décidé de rajouter à notre objet une manivelle qui permet d'activer la rotation.
Nous avons également décidé d'améliorer le moule central, car nous avons rencontrer plein d'échec durant cette journée avec les enfants.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/roto%20machine%203.png)

### Les modes d'emploi.

Pour faciliter la compréhension des différents prototypes que nous avons mis en place, nous avons décidé de joindre à ceux-ci un mode d'emploi, par technique, inspirés de ceux que l'on peut trouver chez IKEA.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/modes%20d'emploi.png)

## Pré-jury.

Après ce jury de décembre, une série de constats ont été fait sur otre projet.

L'élément principal qui est ressorti était le manque d'organistion dans l'espace de notre atelier de fabrication. En effet, nous avions passé beaucoup de temps sur la création de nos prototypes afin de les rendre les plus fonctionnels possible. Nous avons donc décidé de nous concentrer principalement sur l'organisation même de notre atelier au sein du musée, de l'arrivée des enfants au musée, jusqu'à leur sortie avec les petits objets que nous leur auront fait fabriquer.

Nous avons donc pour cela mis en place 2 fiches explicatives destinées aux accompagnants ou aux professeurs en charge des enfants.

La première de ces deux feuilles, explique tout le déroulement de la visite en comprennant une petite partie que l'on conseille aux professeurs de faire en amont de cette visite afin de sensibiliser les enfants au plastique, ses utilisations et les différentes façon de concevoir un objet.

La seconde feuille concerne uniquement notre atelier de la Fabrico'Plastik. On y explique la mise en place de l'atelier le nombre d'enfants idéal et les différentes étapes à respecter pour chacunes des méthodes de fabrication.

Un second élément qui est ressorti de cette correction, était le manque d'unité et de clarté dans la disposition physique de notre atelier. Afin de clarifier cela et de rendre la l'espace plus lisible pour les enfants, nous avons décidé de travailler avec un code couleur. L'idée était de trouver trois couleurs distinctes afin de séparer les méthodes de fabrication, mais en gradant une impression d'unité pour notre activité. Nous avons donc décidé de prendre 3 couleurs de la même famille:

- **Rouge pour le rotomoulage**
- **Orange pour l'injection**
- **Jaune pour l'extrusion**

Pour finir les améliorations de notre atelier, nous avons fabriquer un meuble de rangement inspiré d'une boîte à outils qui pourrait se retrouver dans n'importe quel atelier de fabrication. Ce petit meuble sur roulettes à été dimensionné afin de pourvoir contenir tout le nécessaire pour un atelier avec 14 enfants. que ce soit les machines, les moules, le matériau à travailler, ou encore tous les petits ustensiles utiles aux activités, tout y trouve sa place.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/meuble%20rangement-min.png)

En parallèle, nous avons également continué de perfectionner nos différents prototypes sur base des remarques faites au pré-jury mais aussi sur base de nos constats personnels.

### L'injection.

Pour cette méthode ci, nos moules étaient déjà fonctionnels lors du pré-jury et ils ne nécessitaient donc pas spécialement d'être retravaillés. Nous avons malgrés tout décidé de les aggrandir légèrement pour rendre l'étape d'empilement plus accessible pour les enfants. Nous avons également décidé d'arrondir chacun des bords de nos moules pour éviter qu'un enfant ne se griffe ou se blesse.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/injection%20final-min.png)

### L'extrusion.

Pour l'extrusion nous avions fabriqué, la veille du pré-jury, un nouveau modèle de machine qui venait accompagner les moules en tubes pour pemettre aux enfants d'exercer plus de pression afin de leur faciliter la tâche. Nous étions alors au stade du prototype sale qui permettait de montrer la forme attendue ainsi que son fonctionnement.

À partir de ce premier prototype, il y avait évidement de nombreux éléments à améliorer. Tout d'abbord, il a fallut améliorer la barre verticale où allait être appliquée la pression. Nous avons donc décidé pour cela de changer sa matérialité pour nous tourner vers du métal afin d'augmenter considérablement sa résistance. Nous avons alors réalisé que cet élément métalliques n'était pas optimal pour la prise en mains des enfants donc nous lui avons ajouté une poignée afin de leur indiquer l'endroit à saisir. Nous avons après un test, fait le constat que toutes les pièces qui subissaient une pression nécessitaient d'être en métal afin d'assurer leurs résistance.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/extrusion%202%20final-min.png)

Une autre amélioration que nous avons décidé d'apporter à ce prototype, a été de rendre la face d'extrusion amovible afin de pouvoir changer les formes si nécessaire.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/extrusion%20final-min.png)

### Le rotomoulage.

Pour cette dernière technique, nous avions remarqué trois éléments à améliorer. Le premier était la tige filletée sur le dessus de la machine que nous avons décidé de changer contre une simple tige afin de supprimer les décalages qu'elle engendrait. La seconde modification a été la poignée qui était trop présente et pas du tout adaptée pour les enfants. nous avons donc décidé de la réduire et d'ajouter, comme pour l'extrusion, un petit élément de couleur pour indiquer aux enfants où appliquer une force. Enfin la dernière chose que nous avons voulu améliorer, a été le système d'accroche du moule dans le cadre central. En effet, au départ il était beaucoup trop long et fastidieu à mettre en place. Nous avons donc gardé nos cordes pour en faire un filet sur lequel nous sommes venus coudre une poche dans laquelle glisser le moule ce qui est évidemment bien plus rapide et accessible pour des enfants.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/projet%20final/roto%20final-min.png)

### Les modes d'emploi.

Pour les modes d'emplois, nous avons pris la décision d'en créer de deux catégories différentes, une pour les enfants, contenant uniquement des illustrations, et une pour les adultes, avec pour celle-ci quelques explication écrites.
