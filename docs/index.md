# Qui suis-je?

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Index/247030512_251158200261382_2654865052986220143_n.jpg)

Je m'appelle Maxime Wauthij, j'ai 24ans et je suis actuellement étudiant en MA2 en architecture à l'ULB la Cambre Horta.

## Mon parcours.

 A mon arrivée à l'université je ne savais pas trop vers quelles études me diriger. J'ai commencé par deux années en polytechnique avant de me rendre compte que ça ne me convenais pas, c'est pourquoi je me suis ensuite redirigé vers les études d'architecture où j'arrive maintenant en dernière année de master.
 Ce qui me plait le plus dans mes études et cela depuis la première année, c'est l'aspect manuel et concret de ce cursus. C'est pour cette raison que j'ai choisi l'option Design module 1 au premier quadrimestre de ma MA1. Vous trouverez [ICI](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/maxime.wauthij/) mes travaux de l'année passée. J'ai donc décidé cette année de me réinscrire dans l'option Design mais cette fois-ci au module 3.
