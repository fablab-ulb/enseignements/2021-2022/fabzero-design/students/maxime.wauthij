## Semaine 3: visite approfondie de l'expo.

### Petite parenthèse.

Nous avons d'abbord commencé cette journée en faisant un petit jeux proposé par Victor. Ce petit jeux a pour nom: Les statégies obliques. Le principe est simple, chacun à notre tour, nous avons tiré une carte dans un paquet et sur chacune d'entre elles, il y avait une phrase dont l'objectif était de nous amener à une réflexion personnelle.

### La visite.

Cette visite nous a été donnée par Cristina et s'attarde sur des éléments plus techniques des objets afin de complèter la visite de Terry de la semaine passée.

#### Espace n°1.

##### Chaise Universale : Joe Colombo.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/joe%20colombo.jpg)

Conçue en ABS injecté, cette chaise est designée pour être empilable et que sa hauteur soit ajustable. Elle est un exemple d'objet issu d'un moule très couteux avec lequel on a produit en masse dans le but de le rentabiliser. Certain détails, comme le trou au centre, sont dus au moule.

#### Espace n°2.

##### Lampe de Galanti.

![]()

Cette lampe est constituée d'un module en cylindre qui peut être répliqué et agencé de plusieurs manières différentes. Cela permet de créer, sur base d'un seul moule toute une série d'objets. Cette modularité est primordiale à l'époque car elle permet de créer des objets personnalisables tout en restant dans la production de masse. L'avantage est qu'avec ce  genre d'objets, on peut intégrer l'utilisateur au procédé de fabrication en lui offrant l'idée que son objet est unique.

##### Valentine : Ettore Sottsass.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/ecrire.JPG)

Cette machine à écrire est réalisée en ABS coloré et son design est total il comprend l'objet, son packaging et ssa fonction. Cet objet apparaît dans les années 60 lors de l'essort du plastique et il est donc de faible coût et très accessible pour un grand nombre de personnes. La mode de cette époque est le goût pour le voyage, on tente donc d'empaqueter un maximum d'objets afin de les rendre transportable et cela est rendu très facile par le plastique.

#### Espace n°3.

##### Pantone.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/porte%20a%20faux.jpg)

On retrouve sur ce podium l'évolution de la chaise en porte-à-faux par Pantone. V1, on limite la quantité de matière pour n'avoir qu'un pied mais l'assise est trop lourde et finit par casser. V2, on change le matériau mais ça ne suffit pas donc on ajoute des nervures sur la face inférieure afin de la rendre plus résistante. V3, on change cette fois la forme vers quelque chose de beaucoup plus continu et organique ce qui rend la chaise plus légère et permet donc de retirer les nervures. V4, retour à la forme d'origine grâce aux améliorations apportées au matériau et à sa nouvlle légèreté. on voit aussi apparaitre toute une série de nouvelles couleurs.

#### Espace n°4.

##### Dondolo : Leonardi & Stagi.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/dondolo.jpg)

Cette chaise à bascule, que l'on peut voir au centre de l'image, est une véritable prouesse technologique car elle est le résultat d'une extrusion sur le côté réalisée en fibre de verre. Elle donnerait à son utilisateur l'impression d'être en apesanteur.

#### Espace n°5.

##### Le plastique transparent.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/transparents.jpg)

Le plastique transparent permet d'offrir une incroyable impression de légèreté comme par exemple ce canapé dont le moule pesait environ 30T et qui lui même est très lourd malgré son apparence quasi volatile.

#### Espace n°6.

##### Radical Design.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%203/IMG_1100.JPG)

Dans les années 60/70, le Radical Design naît en protestation au capitalisme de l'époque. L'idée va être de réaliser des objets en plastique mais en série limitée pour s'opposer à la production de masse. Ces objets en mousse ont été réalisés pour la pluspart, entièrement à la main.

##### La résine : Pratt.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/resine.JPG)

Il a réalisé diverses expériences pour essayer de comprendre comment exploiter le matériau notamment en laissant couler la résine. En modifiant la quantité de polyuréthane, la chaise se solidifiait de plus en plus. Malgré tout, la texture finale reste assez étrange au toucher : un peu molle et colle un peu. Ses pièces sont uniques, car il demandait aux ouvriers de choisir le mélange des couleurs ; il faisait de l'anti-design.
