## Semaine 1: premier contact avec le musée.

Pour cette première journée, nous sommes allés au musée ADAM pour rencontrer les gens qui y travaillent et nous familiariser avec les lieux.
Ce musée a vu le jour en 2015 et possède une collection de plus de 1000 objets en plastique, tous créés entre 1950 et aujourd'hui.

### 1 - La Plasticotek et LAB

Ce premier espace du musée, amménagé comme une salle de classe, a pour but d'expliquer l'aspect plus technique du plastique.
Les plastiques se regroupent en 3 familles:
- **thermoplastiques**  
- **thermodurcissants**  
- **élastomères**

On y explique aussi de la manière la plus claire possible les différents procédés qui sont utilisés pour la création des objets.
On en compte 5 :
- **injection**  
- **extrusion**  
- **soufflage**
- **rotomoulage**  
- **thermoformage**

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%201/IMG_20210930_132817.jpg)
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%201/IMG_20210930_132821.jpg)
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%201/IMG_20210930_132825.jpg)

Pour plus d'information sur ces méthodes ou la plastique en général voici un [C'est pas sorcier](https://www.youtube.com/watch?v=irFEnEZhlNM) très clair sur le sujet.

### 2 - Les salles d'exposition

On trouve en effet dans le musée 2 salles d'exposition une dédiée aux expositions temporaires, qui était vide lors de notre visite, et la salle qui contient l'exposition permanente du musée.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%201/IMG_20210930_144443.jpg)

Lors de notre visite on a eu la chance de croiser une personne chargée d'identifier les différents plastiques qui composent les objets du musée. Cette identification est possible grace à une boîte d'échantillons, nos 5 sens, et [ce site](https://www.designmuseumgent.be/fr/musee), du musée de Gand qui propose une série de questions et de points à analyser sur les objets afin d' en identifier les composantes.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%201/IMG_20210930_102715.jpg)

Les objets de l'exposition permanente sont le plus possible changer afin d'éviter une trop grande exposition à la lumière qui entraine une décoloration comme on peut le voir ci-dessous.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%201/IMG_20210930_105100.jpg)

### 3 - La réserve

C'est dans cette salle, le plus souvent plongée dans le noir et interdite d'accès au publique que sont stockés les objets qui ne sont pas exposés.
Tous ces objets y sont stockés dans de grandes étagères métalliques en fonction du type de plastique qui les compose.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%201/IMG_20210930_110155.jpg)
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%201/IMG_20210930_105254.jpg)
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%201/IMG_20210930_110228.jpg)
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%201/IMG_20210930_111834.jpg)
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%201/IMG_20210930_110751.jpg)
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%201/IMG_20210930_112810.jpg)
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%201/IMG_20210930_113102.jpg)
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%201/IMG_20210930_113244.jpg)
