## Semaine 2: visite guidée du musée.

 Pour cette deuxième journée au musée, nous avons commencé par un brainstorming dans lequel nous proposions chacuns nos premières idées pour le projet de rendre le musée accessible à des enfants agés entre 6 et 12 ans.

 Ensuite nous avons eu une visite plus complète de l'exposition pour se familiariser avec le musée.

### Exposition principale : Le plasticarium de Philippe Decelle.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/IMG_20211007_151119.jpg)

En 2014, le musée de l'Atomium rachète la collection d'objets en plastique de Philippe Decelle, un passionné du PMMA qui s'est pris d'intérêt pour tous les objets en plastique datant de la période d'après guerre, une époque où le plastique est un matériau révolutionnaire car il permet la fabrication d'une multitude d'objet de manière très rapide et peu couteuse.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/intro.JPG)

Sa collection démarre sur base de trois éléments:

-**Sa passion pour le PMMA**
-**Son achat de la Garden Egg de Peter Ghyczy**
-**Sa découverte de la chaise de Joe Colombo dans les rues de Uccle**

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/joe%20colombo.jpg)

L'exposition est divisée en 6 espaces différents. On ne retrouve pas un seul fil conducteur à travers l'ensemble des zones mais les objets sont regroupés selon différentes thématique ce qui engendre quand même un certain classement temporel.

#### Espace n°1.

Cette Première zone du musée sert de lieux d'introduction. On y trouve plusieurs magazine ou affiches d'époque présentant les expositions qui ont été mises en place avec ces objets en plastiques. On y retrouve également une vidéo de présentation de Philippe Decelle présentant le personnage, sa passion et les débuts de sa collection. Enfin le premier objet de la collection, la chaise de Joe Colombo.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/affiches.jpg)

#### Espace n°2.

La première salle se penche sur le rêve américain. On y retrouve des objets issus de la production industrielle qui a été engendrée par le boom des plastiques entre 1950 et 1960 induit lui-même par la crise de 1929. L'idée de cette période d'après guerre était de revigorer, révitaliser l'environnement américain. Petit à petit des figures du design sont intervenus pour pallier cette revitalisation et donc de remeubler les intérieurs, créer de nouveaux objet, etc... C'est également une grande période d'avancées technologiques. L'une d'elle, permise par l'arrivée du plastique, a été l'ajout d'un boitier à un grand nombre d'objets afin de les rendre transportables.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/t%C3%A9l%C3%A9s.jpg)

Nous sommes aussi dans un changement en termes de personne, au moment des golden sixties plus tard. Ils veulent se défaire des carcans familiaux donc se défaire des gros meubles en bois ou on encastre la télé dedans, des meubles avec lesquels on ne peut pas voyager on est dans une ère qui arrive avec mai 68. Ici cette envie de voyager être plus libre, les objets en plastiques viendront répondre aux demandes de cette nouvelle génération. Par exemple la machine  à écrire en métal devient en plastique transportable. La technique est cachée dans la boite ce qui était impressionnant dans les années 60.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/ecrire.JPG)

Un autre avantage du plastique, c'est son prix. En effet c'est un matériau très bon marché. Le seul élément couteux dans la production d'objets en plastique, c'est le moule, c'est pourquoi on voit apparaître la production de masse afin de rentabiliser le processus de production. On voit également apparaitre une très grande palette de couleurs car il suffit d'ajouter un pigment au plastique. Et tout le monde peut acheter ses nouveaux objets de mobilier car leur prix est très abordable.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/couleurs.jpg)

#### Espace n°3.

Ici on observe une recherche dans la maléabilité de la matière on expérimente pour découvrir ce dont le plastique est capable, les différentes formes qu'il peut offrir.
Les designers vont s'inspirer d'éléments connus comme Wendell Castle que réalise un mobilier en forme de dents, ou encore Gunter Belzig qui lui s'inspire des formes de la nature en les combinant avec une analyse des points de support du corps ce qui donne ces formes très légères et affinées.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/dents.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/nature.jpg)

Podium de design anthropomorphe.
La chaise a été moulé sur un homme et a été produit en série. On parle toujours de la femme objet, ici Ruth Francken transforme le corps de l’homme en un objet sur lequel on peut s’asseoir.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/corps.jpg)

Le dernier podium de cette zone est dédiée à l'évolution du porte-à-faux, procédé technique intéressant car le but est de créer une chaise sans pieds arrières et où la tension est mise sur l’avant de l’assise.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/porte%20a%20faux.jpg)

#### Espace n°4.

On est  ici vers la fin des années 60 où la science-fiction et les premiers pas sur la Lune vont en inspirer plus d'un. Par exemple, la Dondolo de Leonardi et Stagi, donne l'impression d'être en apesanteur lorsqu'on s'assied dessus. On retrouve également ces différents bureaux qui présentent une esthétique très futuriste. Pour la petite anecdote, chaque nouveau président français demande à un designer de créer son bureau.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/dondolo.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/total.jpg)

#### Espace n°5.

Ce nouvel espace est divisé en deux parties qui sont chacune dédiée à une particularité technique du plastique. La première est la transparence qui va permettre de donner aux objets une impression de légèreté.
La deuxième particularité regroupe les gonflables. En effet si on soude 2 feuilles de PVC entre elles et qu'on y injecte de l'air on obtient un mobilier hyper léger, facilement transportable et qui flotte en plus de ça.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/transparents.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/gonflables.jpg)

#### Espace n°6.

Ce dernier espace est divisé en différents podiums qui présentent chacun leur propre thématique.

D'abbord, on retrouve le Radical design. Courant qui se développe plus en architecture qu’en design, et où les designers profitent de la matière pour s’amuser. On retrouve sur ce podium uniquement des objets en mousse polyuréthane et chaque objet est détourné de la fonction première de l'objet qu'il représente. Ces objets sont construits en séies limitées car on veut s'extirper de la production de masse tout en utilisant le plastique.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/mousse.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/mousses.jpg)

On retrouve ensuite une série d'objets en résinne qui est un type de plastique.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/resine.JPG)

Au début Philippe Decelle ne collectionne pas uniquement des éléments de design mais aussi de l’art. Il y a donc une petite partie de l'exposition dédiée à un cabinet de curiosité avec des œuvres d’art que Philippe a collectionné.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/art.jpg)

On arrive ensuite au podium du post-modernisme. L’idée ici, c’est de revenir à des sentiments dans les  objets car ils trouvent que les objets ne sont plus liés aux gens et donc ils veulent recréer un lien entre les utilisateurs et les objets. On observe aussi un changement aussi au niveau des couleurs plus pastel et l'utilisation d’ancien, comme le trône revisité ou la chaise Proust, fauteuil louis XIV moulé en plastique d’Alexandre Mendini.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/post%20mo.jpg)

Podium année 90 avec grande entreprise comme Kartell qui s’internationalise au départ travaille que avec des designer italien et elle ouvre les portes à des designers et faire des collaborations comme Kartell et Starck. Les entreprises vont vouloir redorer leur image et donc faire appel a de nouveau designer pas forcément italien pour créer de nouveau objet avec une nouvelle esthétique.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/chien.JPG)

Développement des nouvelles technologies. On voit apparaitre le premier ordinateur IMac transparent, lien minimalisme et hightech tout ce qui est technique qui est normalement caché est mis en avant, le mécanisme devient ornement.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/minimal.jpg)

On trouve aussi ici, des objets imprimés en 3D, des objets en plastique qui tentent d'imiter le cristal, ou encore le fauteuil Starck qui est le premier moulé en un seul tenant et creux à l'intérieur.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/3d.JPG)

Enfin, le dernier podium de l'exposition basé sur la thématique de la récupération. On retrouve le fauteuil en plastique recyclé d'Alain Gilles, les chaises en bouteilles de coca recyclées inspirées directement de la chaise en aluminium d’Emeco produite pour les navires et sous-marins de la NAVY et une chaise imprimée en 3D avec comme matériau principal des frigos.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/recycl.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/coca.jpg)

On trouve également ici une nouvelle vague de design lié à l'artisanat et la recherche sur les matériaux. Comme la gravity stool qui mélange émaille de fer et plastique, ou encore un duo de designers français qui travaille avec une communauté du Burkina Faso où ils récupère du caoutchouc de pneux pour fabriquer des sacs et aussi le fauteuil du designer africain Dialo où un fil mauve vient entourer une structure en fer.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/limaille.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%202/frigo.jpg)
