## Semaine 4: rencontre avec Steph, Flo et Yan.

Cette semaine nous avons fait la rencontre de gens qui ont l'habitude de travailler avec des enfants pour avoir leurs avis sur la manière d'aborder ce projet.

Nous avons donc rencontré Yan, le professeur de la classe de l'école Decroly avec laquelle nous allons travailler, ainsi que Flo et Steph, créatrices du Steamlab, qui organise des activité de sensibilisation à la 3D pour des enfants de l'enseignement primaire.

Après cette rencontre, nous leur avons fait une visite du musée où nous les modules 3, avions le rôle de guide. Après cette visite, nous avons écouté leurs ressentis et différents points en sont sortis:

- **Manque d’un fil conducteur**
- **Temps d’attention des enfants est plus courts donc il faut adapter la visite**
- **Modeler l’histoire pour l’adapter aux enfants**
- **Nécessaire de sélectionner des pièces qui résume un ensemble**
- **Faire des liens entre objet, histoire et éléments de leurs quotidien**
- **Essayer de surprendre pour capter l’attention**
- **Se concentrer sur des anecdotes parlantes**
- **Les enfants doivent être acteur et actif**
- **Atelier bricolage serait intéressant**

Suite à ça nous avons expliqué à tours de rôle nos premières idées sur les activité que nous comptions proposer aux enfants de la classe de Yan. De leurs réactions a découlé une discusion qui a menée à mettre en place une liste d'objets de base qui seraient présentés aux enfants.

Cette liste est la suivante:

- **Caillou et herbe de Gufram**
- **Plateau de Gaetano Petche**
- **Fauteuil gonflable de kha**
- **Ghost chair**
- **Sphere isolement**
- **Dondolo Cesare et Stagi**
- **Panton**
- **Dents de Wendell Castle**
- **Vêtement, textile**
- **Fauteuil half and half**
- **Chaise homme de Ruth Vrancken**
- **Télés portables**
