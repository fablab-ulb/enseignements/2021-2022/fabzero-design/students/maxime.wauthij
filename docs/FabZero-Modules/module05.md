## Semaine 5: Les enfants visitent le musée.

Cette semaine nous avons eu notre premier contact avec la classe d'enfants de Decroly. Cette classe se compose de 24 enfants d'environ 7 ans pleins d'énergie.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%205/ph%20exterieure.jpg)

### Déroulement de la journée.

La journée s'est déroulée en 3 parties.

#### 1 : Rencontre avec les enfants.

La classe est arrivée vers 10:00 au musée. A leur arrivée, nous nous sommes regroupés dans la salle polyvalente, où Victor leur a fait une petite introduction sur ce qu'est le design et les raisons qui ont poussées les designers à créer leurs objets de tel ou telle manière. Pour cela, il a analysé avec eux certains objets qu'ils rencontrent au quotidien, comme des sac à dos, des chaises ou encore des boîtes à tartines.

#### 2 : Visite de l'exposition.

Une fois cette petite introduction terminée, nous avons séparé les 24 enfants en 4 groupes de 6 personnes.
Une fois les groupes formés, nous sommes chacun partis d'un côté pour faire visiter le musée aux enfants.
Afin de garder une certaine logique entre les différentes visites, nous avions convenu d'une série de 18 objets qu'il fallait présenter aux enfants.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%205/objets%20visite.jpg)

L'objectif de cette visite était de familiariser les enfants avec le design. Pour chacuns des objets de la liste ci-dessus, je leur posais quelques questions sur leurs formes et les raisons de ces formes, mais aussi sur leur méthode de fabrication.
Après 30min environ, j'ai commencé à perdre leur attention jusqu'à ce qu'ils se mettent à jouer dans le musée. J'ai donc décidé de passer à la deuxième phase de la visite qui consistait à les faire dessiner un ou plusieurs objets de leurs choix.
Cette partie créative était assez interessante car ils avaient tendance à se diriger vers les mêmes objets. Est-ce du à la forme des objets qui attire l'oeil ou juste à un effet de groupe au sein de la classe.

#### 3 : Retour et avis de enfants.

Vers 13:00 après une pause midi bien méritée, on a demandé aux enfants de se réunir dans la Plasticotek à l'arrière du musée afin d'avoir leur retour sur la matinée qu'ils avaient passé au musée.
L'élément qui est le plus ressorti durant cet échange était leur envie de toucher et de manipuler la matière et les objets. Un des moments où ils étaient les plus actifs, c'était lorsque nous leurs avons fait passer un sachet contenant des grains de plastique, qui représente la matière première de quasi tous les objets du musée.
Un autre moment que j'ai trouvé assez interessant, c'est lorsque nous leurs avons permis de se lever à la fin et qu'ils ont enfin pu toucher aux objets imprimés en 3D sur la table au centre de la pièce. A cet instant ils se sont tous rués sur ces petits objets et les mis en lien avec ce qu'ils avaient vu le matin dans le musée.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/-/raw/main/docs/images/Module%205/ph%20interieur.jpg)
